import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  typeValidationForm: FormGroup;
  typesubmit: boolean;
  breadCrumbItems: Array<{}>;

  constructor(public formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Forms' }, { label: 'Form Validation', active: true }];

    this.typeValidationForm = this.formBuilder.group({
      fiscal: ['', [Validators.required]],
      region: ['', [Validators.required]],
      provincia: ['', [Validators.required]],
      distrito: ['', [Validators.required]],

      email: ['', [Validators.required]],
      celular: ['', [Validators.required]],
      cantidad: ['', [Validators.required]],
    });

  }
    
  typeSubmit() {
    this.typesubmit = true;
  }

  get type() {
    return this.typeValidationForm.controls;
  }



}
