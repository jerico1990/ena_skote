// Table data
export interface Table {
    codigo: number;
    region: string;
    provincia: string;
    distrito: string;
    natural: string;
    piso: string;
    segmento: string;
    estratificacion: string;
}

// Search Data
export interface SearchResult {
    tables: Table[];
    total: number;
}
