import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MustMatch } from './validation.mustmatch';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {
  typeValidationForm: FormGroup;
  typesubmit: boolean;
  breadCrumbItems: Array<{}>;

  constructor(public formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Forms' }, { label: 'Form Validation', active: true }];

    this.typeValidationForm = this.formBuilder.group({
      geo: ['', [Validators.required]],
      region: ['', [Validators.required]],
      provincia: ['', [Validators.required]],
      distrito: ['', [Validators.required]],
      natural: ['', [Validators.required]],
      piso: ['', [Validators.required]],
      segmento: ['', [Validators.required]],
      especifica: ['', [Validators.required]],
      grilla: ['', [Validators.required]],
      segmento_empresa: ['', [Validators.required]],
      nativa: ['', [Validators.required]],
      cercano: ['', [Validators.required]],
      obs: ['', [Validators.required]],
    });
  }

  typeSubmit() {
    this.typesubmit = true;
  }

  get type() {
    return this.typeValidationForm.controls;
  }
  

}
