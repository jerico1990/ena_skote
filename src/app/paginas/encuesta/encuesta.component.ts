import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.scss']
})
export class EncuestaComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  isCollapsed: boolean;
  constructor() { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Parcela' }, { label: 'ENCUESTA E.N.A.', active: true }];

    // Collapse value
    this.isCollapsed = false;
  }

}
