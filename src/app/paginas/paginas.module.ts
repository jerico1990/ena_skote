import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PaginasRoutingModule } from './paginas-routing.module';
import { ListaComponent } from './parcela/lista/lista.component';
import { RegistroComponent } from './parcela/registro/registro.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { LoaderService } from '../core/services/loader.service';
import { LoaderInterceptorService } from '../core/services/interceptors/loader-interceptor.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { PerfilComponent } from './perfil/perfil.component';
import { EncuestaComponent } from './encuesta/encuesta.component';
import { UIModule } from '../shared/ui/ui.module';
import { BrowserModule } from '@angular/platform-browser';
import { UnoComponent } from './encuesta/capitulos/uno/uno.component';
import { DosComponent } from './encuesta/capitulos/dos/dos.component';
import { TresComponent } from './encuesta/capitulos/tres/tres.component';
import { CuatroComponent } from './encuesta/capitulos/cuatro/cuatro.component';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 0.3
};

@NgModule({
  declarations: [ListaComponent, RegistroComponent, PerfilComponent, EncuestaComponent, UnoComponent, DosComponent, TresComponent, CuatroComponent],
  imports: [
    CommonModule,
    // BrowserModule,
    NgbModule,
    UIModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    HttpClientModule,
    PaginasRoutingModule,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true }
  ]
})
export class PaginasModule { }
